build:
	deno run --allow-write --allow-read --unstable build.ts

fmt:
	deno fmt

# manually bump
publish:
	npm publish --access restricted
