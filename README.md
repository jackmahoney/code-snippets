# Code Samples

Deno CLI to build src files of examples into a js structure.

Is included by MS2-App to show code samples.

## Deno vs Node

- deno is used to build a json file in dist from src folder
- npm is used to publish the built config file
