export type Config = {
  outputDir: string;
  outputPathApp: string;
  outputPathLanding: string;
  examples: ExampleIn[];
};

export type ExampleIn = {
  title: string;
  snippets: SnippetIn[];
};

export type SnippetIn = {
  language: Language;
  sourceFile: string;
};

export enum Language {
  JS = "javascript",
  // CSHARP = "csharp",
  // JAVA = "java",
  // PHP = "php",
  // RUBY = "ruby"
}

export type LanguageDescription = {
  id: Language;
  prism: string;
  label: string;
};

export type ExampleOut = {
  title: string;
  snippets: SnippetOut[];
};

export type SnippetOut = {
  language: Language;
  sourceCode: string;
};

// config.json for app
export type Output = {
  examples: ExampleOut[];
  languages: LanguageDescription[];
  titles: string[];
};

export type LandingItem = {
  title: string;
  code: string;
};

export type LandingLang = {
  title: string;
  lang: string;
  items: LandingItem[];
};
// landing.json for landing
export type Landing = {
  langs: LandingLang[];
};
