import config from "./config.ts";
import {
  ExampleOut,
  Landing,
  Language,
  LanguageDescription,
  Output,
} from "./_types.ts";
import { ensureDir } from "https://deno.land/std/fs/mod.ts";
import flatten from "https://deno.land/x/denodash/src/array/flatten.ts";
import { assertArrayIncludes } from "https://deno.land/std/testing/asserts.ts";
import { stringify as toYaml } from "https://deno.land/std/encoding/yaml.ts";
console.log("--- START ---");
await ensureDir(config.outputDir);
const entries: ExampleOut[] = await Promise.all(
  config.examples.map(async (example) => {
    return {
      title: example.title,
      snippets: await Promise.all(example.snippets.map(async (snippet) => {
        try {
          return {
            language: snippet.language,
            sourceCode: await Deno.readTextFile(snippet.sourceFile),
          };
        } catch (e) {
          throw new Error(`Failed for snippet '${snippet.sourceFile}': ${e}`);
        }
      })),
    };
  }),
);

function languageDescriptions(): LanguageDescription[] {
  return Object.values(Language).map((l) => {
    switch (l) {
      case Language.JS:
      default:
        return {
          id: l,
          prism: "javascript",
          label: "Javascript",
        };
    }
  });
}

const output: Output = {
  examples: entries,
  titles: entries.map((e) => e.title),
  languages: languageDescriptions(),
};

// check before writing
output.examples.forEach((example) => {
  const snippetLangs = example.snippets.map((snippet) => snippet.language);
  assertArrayIncludes(snippetLangs, output.languages.map((l) => l.id));
});

const landing: Landing = {
  langs: languageDescriptions().map((desc) => {
    return {
      title: desc.label,
      lang: desc.prism,
      items: flatten(
        entries.map((entry) =>
          entry.snippets.filter((s) => s.language === desc.id)
        ),
      ),
    };
  }),
};

console.log(`Writing config to ${config.outputPathApp}`);
await Deno.writeTextFile(config.outputPathApp, JSON.stringify(output, null, 2));

console.log(`Writing landing to ${config.outputPathLanding}`);
await Deno.writeTextFile(config.outputPathLanding, toYaml(landing, {}));
console.log("--- DONE ---");
