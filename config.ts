import { join } from "https://deno.land/std@0.101.0/path/posix.ts";
import { Config, Language } from "./_types.ts";

const root = new URL(".", import.meta.url).pathname;
const sourceRoute = join(root, "/src/");

const config: Config = {
  outputDir: join(root, "/dist/"),
  outputPathApp: join(root, "/dist/config.json"),
  outputPathLanding: join(root, "/dist/landing.yaml"),
  examples: [
    {
      title: "Configure client",
      snippets: [
        {
          language: Language.JS,
          sourceFile: join(sourceRoute, "/js/dashboard.js"),
        },
      ],
    },
    {
      title: "Get emails",
      snippets: [
        {
          language: Language.JS,
          sourceFile: join(sourceRoute, "/js/inbox-get-emails.js"),
        },
      ],
    },
    {
      title: "Send email",
      snippets: [
        {
          language: Language.JS,
          sourceFile: join(sourceRoute, "/js/inbox-send.js"),
        },
      ],
    },
    {
      title: "Receive attachments",
      snippets: [
        {
          language: Language.JS,
          sourceFile: join(sourceRoute, "/js/receive-attachments.js"),
        },
        // {
        //   language: Language.RUBY,
        //   sourceFile: join(sourceRoute, "/ruby/receive_attachments.rb")
        // }
      ],
    },
  ],
};

export default config;
