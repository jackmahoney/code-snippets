// npm install --save mailslurp-client
const { MailSlurp } = require("mailslurp-client");
const mailslurp = new MailSlurp({ apiKey: "___apiKey___" });
// create an inbox
const inbox = await mailslurp.createInbox();
