// npm install --save mailslurp-client
const { MailSlurp } = require("mailslurp-client");
const mailslurp = new MailSlurp({ apiKey: "___apiKey___" });
// fetch emails
const emails = await mailslurp.inboxController.getEmails({
  inboxId: "___inboxId___",
});
