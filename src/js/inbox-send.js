// npm install --save mailslurp-client
const { MailSlurp } = require("mailslurp-client");
const mailslurp = new MailSlurp({ apiKey: "___apiKey___" });
// send email and get saved result
const sentEmail = await mailslurp.inboxController.sendEmailAndConfirm(
  "___inboxId___",
  {
    to: [],
    subject: "",
    body: "",
  },
);
