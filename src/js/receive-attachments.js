async function canReceiveAttachment(inboxId) {
  const waitForController = new MailSlurp(config).waitController;

  const email = await waitForController.waitForLatestEmail(
    inboxId,
    30000,
    true,
  );

  expect(email.attachments.length).toEqual(1);

  const emailController = new MailSlurp(config).emailController;
  const attachmentDto = await emailController.downloadAttachmentBase64(
    email.attachments[0],
    email.id,
  );

  expect(attachmentDto.base64FileContents).toBeTruthy();
  expect(attachmentDto.sizeBytes).toBeTruthy();
  expect(attachmentDto.contentType).toBeTruthy();
}
