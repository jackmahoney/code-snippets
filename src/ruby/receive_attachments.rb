wait_opts = {
  inbox_id: inbox_2.id,
  timeout: 30_000,
  unread_only: true
}
email = wait_controller.wait_for_latest_email(wait_opts)

# find the attachments on the email object
expect(email.attachments.size).to be(1)

# download the attachment as base64 (easier than byte arrays for ruby client)
email_controller = MailSlurpClient::EmailControllerApi.new
downloaded_attachment = email_controller.download_attachment_base64(email.attachments[0], email.id)

# extract attachment content
expect(downloaded_attachment.content_type).to eq("text/plain")
expect(downloaded_attachment.size_bytes).to be_truthy
expect(downloaded_attachment.base64_file_contents).to be_truth